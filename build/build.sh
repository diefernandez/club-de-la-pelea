#!/bin/sh
echo "Login to ECR Repository"
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)

echo "Preparation task"
echo "Build Docker Image"
docker build -t club-de-la-pelea .

echo "Push to ECR Repository"
docker tag club-de-la-pelea:latest $AWS_ECR_REPOSITORY/club-de-la-pelea:latest
docker push $AWS_ECR_REPOSITORY/club-de-la-pelea:latest