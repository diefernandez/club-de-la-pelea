const request = require('supertest');
const app = require('../app');
const elastic = require("../src/elasticMiddleware");

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });

  // it('tests connection to elastic', function (done) {
  //   var elasticEndpoint = "http://monitoreo.debmedia.com:9200";

  //   elastic.connect(elasticEndpoint).then(done);
  // });

  // it('sends a sample document to elastic index', function (done) {

  //   var elasticEndpoint = "http://monitoreo.debmedia.com:9200";

  //   elastic.connect(elasticEndpoint).then(() => {
  //     elastic.indexNewMessage({
  //       from: "me",
  //       chatroom: "none",
  //       message: "Do you think it is a good practice to add an actual message from a test?"
  //     }).then(done);
  //   });
  // });
}); 
