var express = require('express');
const elastic = require("../src/elasticMiddleware");
var router = express.Router();
var gitlab = require('../gitlab');

/* GET users listing. */
router.post('/', function (req, res, next) {
    var response = {
        error: "",
        errorKey: ""
    };
    if (!req.body) {
        response.error = "Body is required";
        response.errorKey = "BODY_REQUIRED";
        res.status(400)        // HTTP status 404: NotFound
            .send(response);
    }
    if (!req.body.from) {
        response.error = "from parameter is required";
        response.errorKey = "FROM_IS_REQUIRED";
        res.status(400)        // HTTP status 404: NotFound
            .send(response);
    }
    if (!req.body.chatroom) {
        response.error = "chatroom parameter is required";
        response.errorKey = "CHATROOM_IS_REQUIRED";
        res.status(400)        // HTTP status 404: NotFound
            .send(response);
    }
    if (!req.body.message) {
        response.error = "message parameter is required";
        response.errorKey = "MESSAGE_IS_REQUIRED";
        res.status(400)        // HTTP status 404: NotFound
            .send(response);
    }

    elastic.indexNewMessage(req.body);

    const message = req.body.message;
    /* Parse commands
       Sample command:
       /newissue: "my title" "my description"
    */
    if(message[0] === "/"){

        switch (message.split(":")[0]) {
            case "/newissue":
                let [title, description] = message.match(/\'[^\']*\'/gi);
                gitlab.createIssue(title, description, undefined, undefined, function (issue) {
                    res.send(issue);
                }, function (err) {
                    res.status(500).send(err);
                });
                break;
            case "/getissues":
                let [command, state] = message.split(" ");
                gitlab.listIssues(state||'opened', function (issues) {
                    res.send(issues);
                }, function (err) {
                    res.status(500).send(err);
                });
                break;
            default:
                res.send(req.body);
                break;
        }
    } else {
        res.send(req.body);
    }

});

module.exports = router;
