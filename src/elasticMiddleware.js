const INDEX_NAME = "fight-club";
const {Client} = require('@elastic/elasticsearch');
let clientConnection;

const isResponseSuccessful = function (resp) {
    if (resp.statusCode) {
        if (resp.statusCode >= 200 && resp.statusCode < 300) {
            return true;
        }
    }
    return false;
};

const parseDocumentFromRequestBody = function (body) {
    let document = {};
    document.from = body.from;
    document.chatroom = body.chatroom;
    document.message = body.message;

    return document;
};

module.exports.connect = function connect(endpoint) {
    clientConnection = new Client({
        node: endpoint,
        auth: {}
    });

    return new Promise(((resolve, reject) => {
        clientConnection.indices.exists({
            index: INDEX_NAME
        }).then(resp => {
            if (isResponseSuccessful(resp)) {
                console.debug("Index already created. Todo flama");
                resolve();
            } else {
                console.debug("Index not created. Doing it now...");
                clientConnection.indices.create({
                    index: INDEX_NAME
                }).then(resp => {
                    if (isResponseSuccessful(resp)) {
                        console.log("Index created. Enjoy.");
                        resolve();
                    } else {
                        console.error("It failed!");
                        reject();
                    }
                });
            }
        })
    }));

};

module.exports.indexNewMessage = function indexMessage(data) {
    const body = parseDocumentFromRequestBody(data);

    return new Promise((resolve, reject) => {
        if (!clientConnection) {
            console.error("You need to call 'connect' before sending data");
            reject();
        }
        clientConnection.index({
            index: INDEX_NAME,
            body: body
        }).then(resp => {
            if (isResponseSuccessful(resp)) {
                console.debug("Entry added with id:", resp.body._id);
                resolve();
            } else {
                console.error("Failed to add document, sorry m8");
                reject();
            }
        });
    });
};


