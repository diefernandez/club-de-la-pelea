const {Gitlab} = require('gitlab');

// El token lo creas aca: https://gitlab.com/profile/personal_access_tokens
const api = new Gitlab({
    host: "https://gitlab.com/",
    token: "bi4-BoFAR6o-hVSxsv6E"
});

const listIssues = function(state, cb, err){
    api.Issues.all({projectId: 15460903, state: state})
        .then(cb)
        .catch(err);
};

const createIssue = function(title, description, labels, milestoneId, cb, err){
    // labels y milestones todavia no soportado
    api.Issues.create(15460903, {
        title: title,
        description: description,
        labels: labels,
        milestone_id: milestoneId
    }).then(cb)
      .catch(err);
};

module.exports = {
    listIssues: listIssues,
    createIssue: createIssue
};